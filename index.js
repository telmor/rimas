var express = require('express');
var util = require('util');
validator = require('express-validator');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var swig = require('swig');

const DB_PATH="data/words.db";

var WORDS_COUNT = 10100624;

app.use(express.static('static'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

var searchOptions = {
  // inicio
  '1': '%s%%',
  // meio
  '2': '%%%s%%',
  // fim
  '3': '%%%s'
};

app.get('/', function(req, res) {
  res.render('index');
});


app.get('/words', function(req, res) {
  var tipo = req.query.tipo;
  var texto = req.query.texto.trim();

  var likefilter = searchOptions[tipo];

  if(likefilter && texto) {
    var filter = util.format(likefilter, texto);
    getAllWordsLike(filter, 1000, 0, function(words) {
      res.render('words', {
        words: words
      });
    });
  } else {
    res.status(400).send('Invalid input');
  }
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});

// WHERE text LIKE ? LIMIT ?, ?

function getAllWordsLike(filter, limit, offset, cb) {
  var db = new sqlite3.Database(DB_PATH);
  var query = "SELECT text FROM words WHERE text LIKE ? LIMIT ?";

  db.serialize(function() {
    db.all(query, [filter, limit], function(err, words) {
      cb(words);
    });
  });

  db.close();
}